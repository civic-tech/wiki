# Civic Tech Wiki

Das ist die Wissensdatenbank von Civic Tech Schweiz. Lernen sie alles über [Civic Tech](Civic%20Tech.md).

## Usage

### Initialize

1. Create an account on <https://gitlab.com>
2. Join the group <https://gitlab.com/civic-tech> (optional)
3. Install <https://obsidian.md/>
4. Install <https://git-scm.com/>
5. Setup an SSH key pair <https://docs.gitlab.com/ee/ssh/#generate-an-ssh-key-pair>.
6. Fork the *Civic Tech Wiki* repository (optional)
7. Clone the *Civic Tech Wiki* repository

```bash
git clone git@gitlab.com:civic-tech/wiki.git civic-tech-wiki
```

8. Open the folder with Obsidian.

![open vault](open%20vault.png)

### Synchronise

The repository is synchronised using the Obisidian Git plugin.

**Pull** - This will update the local repository:

![](git%20pull.png)

* Press <kbd>ctrl + p</kbd>
* Select and enter `Obisdian Git: Pull from remote repository`

**Push** - Your changes will be committed and pushed to <https://gitlab.com/civic-tech/wiki>:

![git push](git%20push.png)

* Press <kbd>ctrl + p</kbd>
* Select and enter `Obisdian Git: Create backup`

